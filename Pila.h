#ifndef PILA_H
#define PILA_H
using namespace std;

//Librerias necesarias
#include <string>

class Pila {
    public:
        //Variables
        int max = 0;
        int *pila = NULL;
        int tope = 0;
        bool band = true;
        
        //Constructor
        Pila();
        Pila(int max);       
        
        //Metodos
        void pila_vacia();
        void pila_llena();
        void push(int dato);
        void pop(int dato);
        void ver_pila();

};
#endif
