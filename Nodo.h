#ifndef NODO_H
#define NODO_H
using namespace std;

//Librerias necesarias
#include <string>

class Nodo {
    public:
        //Variables
        int dato = 0;
        Nodo *siguiente = NULL;

        //Constructor
        Nodo();
        Nodo(int dato); 
    
};
#endif
