/* Escriba un programa que implemente los algoritmos para manipular y mostrar una pila de datos
enteros. Debe utilizar obligatoriamente clases y arreglos en su diseño y Makefile para la compilación.
Suba a la plataforma el link del proyecto en algún repositorio Git. */

#include <iostream>
using namespace std;

#include "Pila.h"

void menu() {
    cout<<"\n\t\t << Menu >>\n\n";
    cout<<" 1. Agregar/push                              "<<endl;
    cout<<" 2. Remover/pop                               "<<endl;
    cout<<" 3. Ver pila                                  "<<endl;
    cout<<" 4. Salir                                     "<<endl;
    cout<<"\nIngrese opcion: ";
}

int main(int argc, char **argv) {

    int op;
    int max;
    cout << "Digite maximo de la pila: ";
    cin >> max;  
    Pila pila = Pila(max);
    int variable;
    
    do{
        menu();  
        cin>> op;
 
        switch(op){
            
            case 1:
                cout<< "\n\t\t  -Agregar/push-\t\n";
                cout << "Digite numero que desee agregar a la pila: ";
                cin >> variable;
                pila.push(variable);

            break;
 

            case 2:
                cout<< "\n\t\t  -Remover/pop-\t\n";
                cout << "Digite numero que desee eliminar de la pila: ";
                cin >> variable;
                pila.pop(variable);
            break;
                 
 
            case 3:
                cout << "\n\t\t  -Ver pila-\n\n";
                pila.ver_pila();

            break;
           
        }
 
    }while(op!=4);
    
    return 0;
}